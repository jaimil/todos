## Todos

[![License](https://poser.pugx.org/leaphly/cart-bundle/license.png)](https://packagist.org/packages/leaphly/cart-bundle)

Todos is a simple app to manage day-to-day tasks, in form of simple Todos. The process is easy to follow.

1. Add a task
2. Finish it
3. Mark it complete
4. Rinse and repeat

![image](https://dl.dropboxusercontent.com/u/25306307/GitHub/Repos/Todos/Todos.jpg)

## Getting Started

The following steps will get you set up with a fully functional Todos app, including dummy data.

### Step 1: Download (Clone repository)

The first step is to download the app, by cloning the repository using `git`.

Use the following command in Terminal: `git clone https://github.com/jaimil/Todos.git`

### Step 2: Install

The app requires downloading of all necessary packages and libraries. Visit `Todos` directory and execute the following command in Terminal: `composer install`

### Step 3: Create Database

Create a MySQL database, and note down the credentials.

### Step 4: Database Configuration File

Using your favourite editor, open `Todos` directory. Within it, create `.env.development.php` file for Environment and Database configuration, with the following content:

```php
<?php

return array(

    'DB_HOST' => 'localhost',
    'DB_NAME' => 'todos',
    'DB_USER' => 'root',
    'DB_PASS' => '1234'

);
```

Change `DB_HOST`, `DB_NAME`, `DB_USER` and `DB_PASS` to match your database credentials.

### Step 5: Migrate and Seed

The database schema needs to be migrated and populated with dummy data. Tackle both steps by entering the following command in Terminal: `php artisan migrate --seed`

### Step 6: Run!

The setup is now complete! To run the application, visit `Todos` directory, and execute the following command in Terminal: `php artisan serve`

Visit the following link to access Todos:

> [Todos (http://localhost:8000)](http://localhost:8000)

## App Structure

Routes:

- `routes.php` (app/routes.php)

Models:

- `Todo.php` (app/models/Todo.php)

Views:

- `layouts/master.blade.php` (app/views/layouts/master.blade.php)
- `todos/index.blade.php` (app/views/todos/index.blade.php)
- `todos/create.blade.php` (app/views/todos/create.blade.php)
- `todos/edit.blade.php` (app/views/todos/edit.blade.php)
- `partials/_notification.blade.php` (app/views/partials/_notification.blade.php)

Controllers:

- `TodosController.php` (app/controllers/TodosController.php)
- `AjaxController.php` (app/controllers/AjaxController.php)

Core:

- Providers
  - `RepositoryServiceProvider.php` (app/Todo/Providers/RepositoryServiceProvider.php)
- Repositories
  - `Todo/TodoRepositoryInterface.php` (app/Todo/Repositories/Todo/TodoRepositoryInterface.php)
  - `Todo/DbTodoRepository.php` (app/Todo/Repositories/Todo/DbTodoRepository.php)
- Services
  - `Validation/Validator.php` (app/Todo/Services/Validation/Validator.php)
  - `Validation/ValidationException.php` (app/Todo/Services/Validation/ValidationException.php)
  - `Validation/TodoValidator.php` (app/Todo/Services/Validation/TodoValidator.php)
  - `Validation/AddTodoValidator.php` (app/Todo/Services/Validation/AddTodoValidator.php)
  - `Validation/EditTodoValidator.php` (app/Todo/Services/Validation/EditTodoValidator.php)

Assets:

- `global.css` (app/public/css/global.css)
- `global.js` (app/public/js/global.js)

## License

The Todos app is open-sourced software licensed under the [MIT License](http://opensource.org/licenses/MIT).