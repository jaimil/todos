(function() {
    if( typeof page !== 'undefined' ) {
        // All Todos
        if( page == 'todos' ) {
            // Toggle status when checkbox is checked / unchecked
            $('[id^="task-checkbox-"]').on('click', function() {
                var checkbox = $(this);
                var task = checkbox.attr('id');
                var taskID = task.substr(task.indexOf('-', 5) + 1);

                // Call Ajax request to toggle status
                $.ajax({
                    type: 'POST',
                    url: '/ajax/todos/toggle-task',
                    data: {id: taskID}
                }).done(function(res) {
                    var completed = (res == 'complete');
                    var taskRow = $('#task-row-' + taskID);

                    // Notify upon success
                    $.notify(
                        (completed) ? 'Marked Complete' : 'Marked Incomplete',
                        {
                            className: 'success',
                            showAnimation: 'fadeIn',
                            showDuration: 250,
                            hideAnimation: 'fadeOut',
                            hideDuration: 250
                        }
                    );

                    // Toggle green (success) highlight on table row, depending on status
                    (completed) ? taskRow.addClass('success') : taskRow.removeClass('success');
                });
            });

            // Delete todo
            $('.delete-todo').on('click', function() {
                // Ask for confirmation
                if( confirm('Are you sure?\n\nThis action cannot be reverted!') )
                    $(this).next().submit();

                return false;
            });
        }

        // Add Task
        if( page == 'add-task' ) {
            var field = $('#task');

            if( ! field.val() )
                field.focus();
        }
    }
})();