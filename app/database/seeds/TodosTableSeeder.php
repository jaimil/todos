<?php

class TodosTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Pull in Faker to generate randomized, dummy data
        $faker = Faker\Factory::create();

        // Create 25 records
        foreach( range(1, 25) as $index ) {
            Todo::create([
                'task' => rtrim($faker->text(64), '.'),
                'status' => rand(0, 1)
            ]);
        }
    }

}