<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        // Seed TODOS table with dummy data
		$this->call('TodosTableSeeder');
	}

}