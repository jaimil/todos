<?php namespace Todo\Services\Validation;

class AddTodoValidator extends Validator {

    // Validation rules for adding todo
    static $rules = [
        'task' => 'required'
    ];

    // Validation messages for adding todo
    static $messages = [
        'task.required' => 'The Task is required.'
    ];

}