<?php namespace Todo\Services\Validation;

class EditTodoValidator extends Validator {

    // Validation rules for editing todo
    static $rules = [
        'task' => 'required'
    ];

    // Validation messages for editing todo
    static $messages = [
        'task.required' => 'The Task is required.'
    ];

}