<?php namespace Todo\Services\Validation;

use Validator as V;

abstract class Validator {

    /**
     * Validate user input.
     *
     * @param $input
     * @return bool
     * @throws ValidationException
     */
    public function validate($input)
    {
        // Create validator
        $validation = V::make($input, static::$rules, static::$messages);

        // Validate - if fails, throw ValidationException
        if( $validation->fails() )
            throw new ValidationException($validation->messages());

        // Otherwise, return true
        return true;
    }

}