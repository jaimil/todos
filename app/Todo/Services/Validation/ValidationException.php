<?php namespace Todo\Services\Validation;

use Exception;

class ValidationException extends Exception {

    protected $errors;

    /**
     * Constructor used to call parent constructor, and assign values.
     *
     * @param string $errors
     * @param string $message
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($errors, $message = '', $code = 0, Exception $previous = null)
    {
        $this->errors = $errors;

        parent::__construct($message, $code, $previous);
    }

    /**
     * Return errors.
     *
     * @return string
     */
    public function getErrors()
    {
        return $this->errors;
    }

}