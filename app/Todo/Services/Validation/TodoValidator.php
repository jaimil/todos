<?php namespace Todo\Services\Validation;

class TodoValidator {

    protected $add_todo_validator;
    protected $edit_todo_validator;

    /**
     * Constructor used for Dependency Injection.
     *
     * @param AddTodoValidator $add_todo_validator
     * @param EditTodoValidator $edit_todo_validator
     */
    public function __construct(AddTodoValidator $add_todo_validator, EditTodoValidator $edit_todo_validator)
    {
        // Assign dependencies
        $this->add_todo_validator = $add_todo_validator;
        $this->edit_todo_validator = $edit_todo_validator;
    }

    /**
     * Validate user input, based on creation or modification of todos.
     *
     * @param $type
     * @param $input
     */
    public function validate($type, $input)
    {
        switch( $type )
        {
            // Validate add todo
            case 'add-todo':
                $this->add_todo_validator->validate($input);
                break;

            // Validate edit todo
            case 'edit-todo':
                $this->edit_todo_validator->validate($input);
                break;
        }
    }

}