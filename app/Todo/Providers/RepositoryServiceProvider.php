<?php namespace Todo\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    public function register()
    {
        // Bind to use todo via database (eloquent)
        $this->app->bind(
            'Todo\Repositories\Todo\TodoRepositoryInterface',
            'Todo\Repositories\Todo\DbTodoRepository'
        );
    }

}