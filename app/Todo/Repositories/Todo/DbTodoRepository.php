<?php namespace Todo\Repositories\Todo;

use Todo;

class DbTodoRepository implements TodoRepositoryInterface {

    protected $todo;

    /**
     * Constructor used for Dependency Injection.
     *
     * @param Todo $todo
     */
    public function __construct(Todo $todo)
    {
        // Assign dependencies
        $this->todo = $todo;
    }

    /**
     * Get paginated result, based on the set number provided.
     *
     * @param int $set
     * @return mixed
     */
    public function getPaginated($set = 10)
    {
        // Grab paginated todos from database
        return $this->todo->orderBy('id', 'desc')->paginate($set);
    }

    /**
     * Retrieve todo based on provided ID.
     *
     * @param $id
     * @return mixed
     */
    public function getByID($id)
    {
        // Grab todo by ID
        return $this->todo->find($id);
    }

    /**
     * Create a new todo.
     *
     * @param $input
     */
    public function create($input)
    {
        // Create new todo
        $todo = new $this->todo;

        $todo->task = $input['task'];
        $todo->status = 0;
        $todo->save();
    }

    /**
     * Update existing todo, based on provided ID.
     *
     * @param $id
     * @param $input
     */
    public function update($id, $input)
    {
        // Update todo with provided ID
        $todo = $this->getByID($id);

        $todo->task = $input['task'];
        $todo->save();
    }

    /**
     * Delete todo, based on provided ID.
     *
     * @param $id
     */
    public function destroy($id)
    {
        // Delete todo with provided ID
        $this->getByID($id)->delete();
    }

}