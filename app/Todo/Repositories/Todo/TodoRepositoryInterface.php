<?php namespace Todo\Repositories\Todo;

interface TodoRepositoryInterface {

    public function getPaginated($set = 10);
    public function getByID($id);
    public function create($input);
    public function update($id, $input);
    public function destroy($id);

}