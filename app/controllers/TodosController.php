<?php

use Todo\Repositories\Todo\TodoRepositoryInterface;
use Todo\Services\Validation\TodoValidator;
use Todo\Services\Validation\ValidationException;

class TodosController extends \BaseController {

    protected $todo;
    protected $validator;

    /**
     * Constructor used for Dependency Injection.
     *
     * @param TodoRepositoryInterface $todo
     * @param TodoValidator $validator
     */
    public function __construct(TodoRepositoryInterface $todo, TodoValidator $validator)
    {
        // Assign dependencies
        $this->todo = $todo;
        $this->validator = $validator;
    }

	/**
	 * Display a listing of the todos.
	 *
	 * @return Response
	 */
	public function index()
	{
        // Grab paginated todos from repository (database)
        $todos = $this->todo->getPaginated(7);

        // Present list of todos
        return View::make('todos.index')->withTitle('Welcome')->withTodos($todos);
	}

	/**
	 * Show the form for creating a new todo.
	 *
	 * @return Response
	 */
	public function create()
	{
		// Present form to add task
        return View::make('todos.create')->withTitle('Add Task');
	}

	/**
	 * Store a newly created todo in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        // Validate add todo
        try
        {
            $this->validator->validate('add-todo', Input::all());
        }

        catch( ValidationException $e )
        {
            return Redirect::back()->withInput()->withErrors($e->getErrors());
        }

        // Create todo
        $this->todo->create(Input::all());

        // Redirect back to Todos, with success
        return Redirect::route('todos.index')->withMessage(['success', '<strong>Hooray!</strong> The todo has been successfully added.']);
	}

	/**
	 * Show the form for editing the specified todo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // Fetch todo with specified ID
		$todo = $this->todo->getByID($id);

        // Present form to edit todo
        return View::make('todos.edit')->withTitle('Edit Task')->withTodo($todo);
	}

	/**
	 * Update the specified todo in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // Validate edit todo
        try
        {
            $this->validator->validate('edit-todo', Input::all());
        }

        catch( ValidationException $e )
        {
            return Redirect::back()->withInput()->withErrors($e->getErrors());
        }

        // Update todo
        $this->todo->update($id, Input::all());

        // Redirect back to Todos, with success
        return Redirect::route('todos.index')->withMessage(['success', '<strong>Hooray!</strong> The todo has been successfully edited.']);
	}

	/**
	 * Remove the specified Todo from database.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // Delete todo
        $this->todo->destroy($id);

        // Redirect back to Todos, with success
        return Redirect::route('todos.index')->withMessage(['success', '<strong>Hooray!</strong> The todo has been successfully deleted.']);
	}

}