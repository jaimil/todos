<?php

use Todo\Repositories\Todo\TodoRepositoryInterface;

class AjaxController extends \BaseController {

    protected $todo;

    /**
     * Constructor used for Dependency Injection.
     *
     * @param TodoRepositoryInterface $todo
     */
    public function __construct(TodoRepositoryInterface $todo)
    {
        // Assign dependencies
        $this->todo = $todo;
    }

    /**
     * Toggles the 'Status' of a Todo.
     *
     * @return string
     */
    public function todoToggleTask()
    {
        // Retrieve todo with specified ID, from database
        $todo = $this->todo->getByID(Input::get('id'));

        // Toggle status
        $todo->status = ($todo->status == 0) ? 1 : 0;
        $todo->save();

        // Return updated status
        return ($todo->status == 1) ? 'complete' : 'incomplete';
    }

}