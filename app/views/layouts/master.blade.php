<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="A simple app to manage Todos.">
    <meta name="author" content="Jaimil Prajapati">

    <title>{{ $title }} | Todos</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css') }}

    <!-- Todo global CSS -->
    {{ HTML::style('css/global.css') }}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="container">
        <div class="header">
            <ul class="nav nav-pills pull-right">
                @if( Route::currentRouteName() == 'todos.index' )
                    <li class="active"><a href="{{ URL::route('todos.index') }}">All Todos</a></li>
                @else
                    <li><a href="{{ URL::route('todos.index') }}">All Todos</a></li>
                @endif

                @if( Route::currentRouteName() == 'todos.create' )
                    <li class="active"><a href="{{ URL::route('todos.create') }}">Create Task</a></li>
                @else
                    <li><a href="{{ URL::route('todos.create') }}">Create Task</a></li>
                @endif
            </ul>
            <h3 class="text-muted">Todos</h3>
        </div>

        <div class="jumbotron">
            <h1>Welcome to Todos</h1>
            <p class="lead">
                Todos is a simple app to manage day-to-day tasks, in form of simple Todos. The process is easy to follow.<br /><br />
                1. Add a task<br />
                2. Finish it<br />
                3. Mark it complete<br />
                4. Rinse and repeat
            </p>
            <br />
            <p><a class="btn btn-lg btn-success" href="{{ URL::route('todos.create') }}" role="button">Add a task now!</a></p>
        </div>

        <div class="row">
            <div class="col-lg-12">
                @yield('content')
            </div>
        </div>

        <div class="footer">
            <p>&copy; Todos 2014</p>
        </div>
    </div> <!-- /container -->

    <!-- jQuery -->
    {{ HTML::script('//code.jquery.com/jquery-2.1.0.min.js') }}

    <!-- Bootstrap core JavaScript -->
    {{ HTML::script('//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js') }}

    @yield('scripts')

    <!-- Todo global JS -->
    {{ HTML::script('js/global.js') }}
</body>
</html>