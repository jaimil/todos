@if( Session::has('errors') )
    <div class="alert alert-danger">
        <strong>Oh snap!</strong> The following error(s) occurred:
        <ul>
            @foreach( $errors->all() as $error )
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if( Session::has('message') )
    <div class="alert alert-{{ Session::get('message')[0] }}">
        {{ Session::get('message')[1] }}
    </div>
@endif