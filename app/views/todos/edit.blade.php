@extends('layouts.master')->withTitle($title)

@section('content')
    @include('partials._notification')
    {{ Form::open(['route' => ['todos.update', $todo->id], 'method' => 'PUT']) }}
        {{ Form::label('task', 'Task *') }}
        {{ Form::text('task', $todo->task, ['class' => 'form-control']) }}
        <br />
        * required
        <br /><br />
        {{ Form::submit('Save Changes', ['class' => 'btn btn-success']) }}
    {{ Form::close() }}
@stop