@extends('layouts.master')->withTitle($title)

@section('content')
    @include('partials._notification')
    {{ Form::open(['route' => 'todos.store']) }}
        {{ Form::label('task', 'Task *') }}
        {{ Form::text('task', null, ['class' => 'form-control']) }}
        <br />
        * required
        <br /><br />
        {{ Form::submit('Add Task', ['class' => 'btn btn-success']) }}
    {{ Form::close() }}
@stop

@section('scripts')
    <script> var page = 'add-task'; </script>
@stop