@extends('layouts.master')->withTitle($title)

@section('content')
    @include('partials._notification')
    @if( $todos->count() )
        <table class="table table-bordered todos">
            <thead>
                <tr>
                    <th>Status</th>
                    <th colspan="2">Task</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $todos as $todo )
                    <tr id="task-row-{{ $todo->id }}" @if( $todo->status == 1 ) class="success" @endif>
                        <td>{{ Form::checkbox(null, null, ($todo->status == 1) ? true : false, ['id' => 'task-checkbox-' . $todo->id]) }}</td>
                        <td>{{ $todo->task }}</td>
                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <span class="glyphicon glyphicon-cog"></span> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="{{ URL::route('todos.edit', $todo->id) }}"><span class="glyphicon glyphicon-edit"></span> Edit</a></li>
                                    <li>
                                        <a href="javascript: void(0)" class="delete-todo"><span class="glyphicon glyphicon-remove"></span> Delete</a>
                                        {{ Form::open(['route' => ['todos.destroy', $todo->id], 'method' => 'DELETE']) }}
                                        {{ Form::close() }}
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $todos->links() }}
    @else
        <div class="alert alert-warning no-todos">
            There are no todos present. Click <strong>Add a task now!</strong> above to add one now!
        </div>
    @endif
@stop

@section('scripts')
    <!-- Notify.js -->
    {{ HTML::script('js/lib/notify.min.js') }}

    <!-- Assign page for global.js -->
    <script> var page = 'todos'; </script>
@stop