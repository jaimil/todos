<?php

class Todo extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'todos';

    /**
     * The fillable columns allowed through mass assignment.
     *
     * @var array
     */
    protected $fillable = ['task', 'status'];

}