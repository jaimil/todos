<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Redirect / to Todos
 */
Route::get('/', function() {
    return Redirect::route('todos.index');
});

/**
 * Todos Routes
 */
Route::resource('todos', 'TodosController', ['except' => ['show']]);

/**
 * Ajax Routes
 */
Route::post('ajax/todos/toggle-task', 'AjaxController@todoToggleTask');